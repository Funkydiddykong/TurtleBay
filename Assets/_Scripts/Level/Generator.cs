﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {

    public float size;

    public GameObject[] plants;
    public float wallDensity;
    public GameObject shark;
    public GameObject[] food;
    public GameObject[] background;

    public int initualsharks;
    public int initualfood;
    public int initualbackground;

    private float plant_raise = 2.0f;

	// Use this for initialization
	void Start () {
        generateWalls();
        spawnMultipleSafe(shark, initualsharks);
        spawnMultipleSafe(food[UnityEngine.Random.Range(0, food.Length)], initualfood);
        spawnMultipleSafe(background[UnityEngine.Random.Range(0, background.Length)], initualbackground);
	for (int i = 0; i < 15; i++) {
            	GameObject plant = plants[UnityEngine.Random.Range(0, plants.Length)];
		Instantiate(plant, new Vector3(UnityEngine.Random.Range(-size, size), plant_raise, UnityEngine.Random.Range(-size, size)), Quaternion.Euler(new Vector3(0, UnityEngine.Random.Range(0f, 360f), 0)), this.transform);		
}
	}

    private void spawnMultipleSafe(GameObject obj, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            safeSpawn(obj);
        }
    }

    private void generateWalls()
    {
        createWall(new Vector3(size, plant_raise, size), new Vector3(size, plant_raise, - size));
        createWall(new Vector3(size, plant_raise, size), new Vector3(- size, plant_raise, size));
        createWall(new Vector3(- size, plant_raise, - size), new Vector3(size, plant_raise, - size));
        createWall(new Vector3(- size, plant_raise, - size), new Vector3(- size, plant_raise, size));
    }

    private void createWall(Vector3 start, Vector3 end)
    {
        for (int i = 0; i < wallDensity; i++)
        {
            float frac = (float) i / wallDensity;
            GameObject plant = plants[UnityEngine.Random.Range(0, plants.Length)];
            Instantiate(plant, Vector3.Lerp(start, end, frac), Quaternion.Euler(new Vector3(0, UnityEngine.Random.Range(0f, 360f), 0)), this.transform);
        }
    }

    internal void spawnShark()
    {
        safeSpawn(shark);
    }

    private void safeSpawn(GameObject obj)
    {
        float safesize = size * 0.9f;
        float x = UnityEngine.Random.Range(-safesize, safesize);
        float z = UnityEngine.Random.Range(-safesize, safesize);

        Instantiate(obj, new Vector3(x, 1, z), Quaternion.Euler(new Vector3(0, UnityEngine.Random.Range(0f, 360f), 0)), this.transform);
    }

    internal void spawnFood()
    {
        //Debug.Log("WHY ME");
        //safeSpawn(food[0]);
        safeSpawn(food[UnityEngine.Random.Range(0, food.Length)]);
    }
}
