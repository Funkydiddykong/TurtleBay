﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class neutralController : MonoBehaviour {
    
    public float ediblerange;
    public float range;
    public float speed;
    public float reactionspeedmultiplier;

    public AnimationClip slowswim;
    public AnimationClip fastswim;
    public AnimationClip death;

    private NeutralType type;
    private GameObject player;
    private PlayerController controller;
    private Animation animationagent;
    private NavMeshAgent agent;
    private Vector3 wandertarget;

    private bool alive = true;

    public enum NeutralType
    {
        Fish,
        Jellyfish
    }
    
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        animationagent = GetComponent<Animation>();
        player = GameObject.FindGameObjectWithTag("Player");
        controller = player.GetComponent<PlayerController>();
        type = (NeutralType)Enum.Parse(typeof(NeutralType), gameObject.tag, true);

        StartCoroutine(targetNewWanderLocation());

    }

    void Update() {
        if (alive) {
            float distance = Vector3.Distance(player.transform.position, transform.position);
            if (distance < ediblerange)
            {
                alive = false;
                controller.ate(type);
                animationagent.CrossFade(death.name);

                StartCoroutine(queueDeath());
            }
            else if (distance < range)
            {
                agent.speed = speed * reactionspeedmultiplier;
                animationagent.CrossFade(fastswim.name);
                Vector3 norm = (transform.position - player.transform.position).normalized;

                agent.SetDestination(transform.position + norm * 2);
            }
            else
            {
                animationagent.CrossFade(slowswim.name);
                agent.speed = speed;
                agent.SetDestination(wandertarget);
            }
        }
    }

    IEnumerator queueDeath()
    {
        yield return new WaitForSeconds(3);

        Destroy(gameObject);
    }

    private IEnumerator targetNewWanderLocation()
    {
        while (true)
        {
            wandertarget = UnityEngine.Random.insideUnitCircle * 30;
            yield return new WaitForSeconds(5);
        }
    }
}
