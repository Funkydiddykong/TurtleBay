﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SharkController : MonoBehaviour {

    public float range;
    public bool agressive;
    public float speed;
    public float reactionspeedmultplier;
    public float attackrange;
    public int damage;

    private Animation animationagent;
    private NavMeshAgent agent;
    private GameObject player;
    private Vector3 wandertarget;

    private PlayerController controller;
    
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        animationagent = GetComponent<Animation>();
        player = GameObject.FindGameObjectWithTag("Player");
        controller = player.GetComponent<PlayerController>();

        StartCoroutine(targetNewWanderLocation());
	}

    void Update()
    {
        float disance = Vector3.Distance(this.transform.position, player.transform.position);
        if (disance < attackrange && agressive)
        {
            animationagent.Play("attack");
            controller.damage(damage);
        }
        else if (disance < range && agressive)
        {
            agent.speed = speed * reactionspeedmultplier;
            agent.SetDestination(player.transform.position);
            animationagent.CrossFade("swim run");
        }
        else
        {
            agent.speed = speed;
            animationagent.CrossFade("swim idle");
            agent.SetDestination(wandertarget);
        }
    }

    private IEnumerator targetNewWanderLocation()
    {
        while (true)
        {
            Vector2 circ = Random.insideUnitCircle;
            wandertarget = transform.position + (new Vector3(circ.x, 0, circ.y)  * 15);
            yield return new WaitForSeconds(5);
        }
    }
}
