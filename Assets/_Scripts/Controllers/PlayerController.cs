﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public Camera playercamera;
    public float forwardforce;
    public KeyCode forwardkey;

    public Slider healthslider;
    public Text scoretext;
    public Text deathtext;

    private Rigidbody ridgedbody;
    private Generator generator;

    private int health;
    private int score;

    public int STARTING_HEALTH;
    private int maxhealth;

    private bool alive = true;

	void Start () {
		ridgedbody = GetComponent<Rigidbody>();
        generator = GameObject.FindGameObjectWithTag("Level").GetComponent<Generator>();
        reset();
    }

    public void reset()
    {
        health = STARTING_HEALTH;
        maxhealth = STARTING_HEALTH;
        score = 0;
        alive = true;
        deathtext.enabled = false;

        updateUI();
    }

    private void updateUI()
    {
        scoretext.text = score.ToString();
        healthslider.maxValue = maxhealth;
        healthslider.value = health;
        if (!alive)
        {
            deathtext.enabled = true;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (alive)
        {
            updateRotation();
            updateForces();
        }

        //Debug.Log("health :" + health + "   score: " + score);
		
	}
    
    public void ate(neutralController.NeutralType type)
    {
        switch(type)
        {
            case neutralController.NeutralType.Jellyfish:
                score += 10;
                if (score % 50 == 0) { generator.spawnShark(); }
                health += 3;
                if (health > maxhealth) health = maxhealth;
                updateUI();
                generator.spawnFood();
                break;
        }
    }

    private bool invul = false;

    public void damage(int amount)
    {
        if (alive)
        {
            if (!invul)
            {
                invul = true;
                health -= amount;
                if (health < -0)
                {
                    alive = false;
                }
                updateUI();
                StartCoroutine(invulFrames());
            }
        }
    }

    IEnumerator invulFrames()
    {
        yield return new WaitForSeconds(1);
        invul = false;
    }


    private void updateForces()
    {
        if (Input.GetKey(forwardkey))
        {
            ridgedbody.AddForce(transform.forward * forwardforce * Time.deltaTime * 100);
        }
    }

    private void updateRotation()
    {
        Vector3 mousepos = playercamera.ScreenToWorldPoint(Input.mousePosition);
        mousepos.y = this.transform.position.y;


        this.transform.LookAt(mousepos);


    }
}
