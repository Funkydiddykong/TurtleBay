﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform player;
    
	void Update () {
        Vector3 playerpos = player.position;

        Vector3 newcamerapos = new Vector3(playerpos.x, this.transform.position.y, playerpos.z);

        this.transform.position = newcamerapos;
	}
}
